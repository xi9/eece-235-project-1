1. Title Page
Name
Course Number
Date
Collaborators


2. Introduction / Background / Problem Statement
Problem statement
Additional background research (if requested when project /
lab was assigned) , and
Experiment design: identify independent variables, dependent variables, controls, etc... (this section is likely on necessary as the projects become more complex)

3. Methods:
Materials, Circuit Design, and Code Design
If the project was 100% code, this section is not needed
(unless specifically requested)
Draw ALL relevant circuit configurations and associated
connections
Include a photo of your project as wired
Annotations preferred
Label all components, hardware, and instruments
If resistors and/or capacitors are used, include measured values

4. Results & Discussion
Present all findings using both visuals and words.
Create tables and graphs to present data.
Must include titles, axis labels, and legends.
Include text to describe what you see in the figures.
Calculate / report items such as frequency, peak‐to‐peak voltages of measured
signals, duty cycle, etc...
Perform necessary calculations in a clear way that states assumptions and relevant equations.

5. Conclusions & Questions
Did you achieve the goal of the project / experiment? Explain what you learned.
Explain any unexpected results.
Answer any additional questions assigned in the project / lab.
6. References
7. Appendix
Include code, supporting formulas, or calculations not found elsewhere


Project Checklist

Assemble and test a Full Adder Circuit (See Figure 3.3 in textbook). 

Your report should include the following:
Intro section with 1 or more references
Discuss the general role of adder circuits and the differences (pros/cons) of various fast adder circuits (2 or 3 others).
Discuss how they are similar to the full adder.
Discuss how you could modify an adder into an adder‐subtractor.
Photo of wired circuit with labels
Full schematic (drawing with gate symbols and signal names)
Demonstrate functionality by putting LEDs on inputs and output
Be sure to use current limiting resistor with LEDs
Hint: Blue, Green, and White LEDs all have a forward voltage of about 3V.
Truth Table (with label and caption)
Karnaugh Map (with label and caption).
Use P1 and P2 and the 4 scope channels to create a
timing diagram
Include this image from the nScope
