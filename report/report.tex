\documentclass{report}
\usepackage{xcolor}
\definecolor{mav-maroon}{HTML}{860037}
\definecolor{mav-gold}{HTML}{FFD200}
\definecolor{mav-darkgold}{HTML}{BF9500}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{microtype}
\usepackage{pdfpages}
\usepackage[style=numeric]{biblatex}
\usepackage{karnaugh-map}
\usepackage{circuitikz}
\usepackage{graphicx}
\usepackage{float}
\usepackage[
    colorlinks=true,
    linkcolor=mav-maroon,
    citecolor=mav-darkgold
    ]{hyperref}
\usetikzlibrary{positioning, fit, calc}  %added for block diagram

% Define typographic struts for tables, as suggested by Claudio Beccari
%   in an article in TeX and TUG News, Vol. 2, 1993.
\newcommand\topstrut{\rule{0pt}{2.6ex}}
\newcommand\bottomstrut{\rule[-0.9ex]{0pt}{0pt}}

\author{
    Olive N. Egbert \& Matthew B. Wilson
}
\title{
    EECE 235:\\
    Assembly and Characterization of a\\
    Discrete Full-Adder Circuit
}
\addbibresource{../sources/digital-logic-book.bib}
\addbibresource{../sources/another-digital-logic-book.bib}

\begin{document}

\maketitle
\date{}

\begin{abstract}
	One of the most important primitive discrete logic circuits is the full-adder, which takes two input bits as well as a carry-in bit and returns the sum of the two input bits as well as a carry-out bit.
	A brief overview of the theory and modern relevancy of the full-adder is given, and a selection of implementations of the circuit is presented.
	Two selected implementations are contrasted, constructed, and characterized relative to the theory upon which they are based.
\end{abstract}

\section*{Theory \& Background}

An adder, in general, is a circuit or system which implements the addition of its inputs.
In the most general case for any digit position when adding two numbers, an adder must input the carry-in from a previous digit position as well as the two digits from the addends.
A full-adder is such a circuit, and implements the two functions \(s_{o}\) and \(c_{o}\) which can be defined as:

\begin{itemize}
	\item \(s_{o}\): the sum of addend input bits \(x_{i},\ y_{i}\) and carry-in bit \(c_{i}\)
	\item \(c_{o}\): the carry-out bit should the sum of \(x_{i},\ y_{i},\ c_{i}\) overflow
\end{itemize}

These functions are described by Table~\ref{tab:adder_truth}.\autocite[p. 127]{brown_fundamentals_2013}

\begin{table}[H]
	\centering
	\begin{tabular}{|c c c | c c|}
		\hline
		\(c_{i}\) & \(x_{i}\) & \(y_{i}\) & \(c_{o}\) & \(s_{o}\)\topstrut\bottomstrut \\
		\hline
		0         & 0         & 0         & 0         & 0\topstrut                     \\
		0         & 0         & 1         & 0         & 1                              \\
		0         & 1         & 0         & 0         & 1                              \\
		0         & 1         & 1         & 1         & 0                              \\
		1         & 0         & 0         & 0         & 1                              \\
		1         & 0         & 1         & 1         & 0                              \\
		1         & 1         & 0         & 1         & 0                              \\
		1         & 1         & 1         & 1         & 1\bottomstrut                  \\
		\hline
	\end{tabular}
	\caption{
		Truth table for functions \(s_{o}\ \&\ c_{o}\) given inputs \(x_{i},\ y_{i}\ \&\ c_{i}\).
	}
	\label{tab:adder_truth}
\end{table}

Transforming these functions into Karnaugh maps yields Figures~\ref{fig:so_karnaugh}~\&~\ref{fig:co_karnaugh} with the implicants of each giving the relations

\begin{align}
	s_{o} & \equiv c_{i}\overline{x_{i}y_{i}} + \overline{c_{i}x_{i}}y_{i} + c_{i}x_{i}y_{i} + \overline{c_{i}}x_{i}\overline{y_{i}} \label{eq:so_implication} \\
	      & \equiv c_{i}\left(\overline{x_{i}y_{i}} + x_{i}y_{i}\right) + \overline{c_{i}}\left(\overline{x_{i}}y_{i} + x_{i}\overline{y_{i}}\right) \nonumber \\
	      & \equiv c_{i}\left(\overline{x_{i}y_{i}} + x_{i}y_{i}\right) + \overline{c_{i}}\left(x_{i}\oplus y_{i}\right) \nonumber                             \\
	      & \equiv c_{i}\overline{\left(x_{i}\oplus y_{i}\right)} + \overline{c_{i}}\left(x_{i}\oplus y_{i}\right) \nonumber                                   \\
	      & \equiv c_{i} \oplus \left(x_{i}\oplus y_{i}\right) \nonumber                                                                                       \\
	      & \equiv c_{i} \oplus x_{i} \oplus y_{i} \label{eq:so_equation}
\end{align}

\begin{align}
	c_{o} & \equiv c_{i}\overline{x_{i}}y_{i} + x_{i}y_{i} + c_{i}x_{i}\overline{y_{i}} \label{eq:co_implication} \\
	      & \equiv x_{i}y_{i} + c_{i}\left(\overline{x_{i}}y_{i} + x_{i}\overline{y_{i}}\right) \nonumber         \\
	      & \equiv x_{i}y_{i} + c_{i}\left(x_{i} \oplus  y_{i}\right) \label{eq:co_equation}
\end{align}

respectively.

\begin{figure}[H]
\centering
\begin{karnaugh-map}[4][2][1][\(x_{i}y_{i}\)][\(c_{i}\)]
\minterms{1,2,4,7}
\autoterms[0]
\implicant{1}{1}
\implicant{2}{2}
\implicant{4}{4}
\implicant{7}{7}
\end{karnaugh-map}
\caption{
	Karnaugh map for the full-adder function \(s_{o}\), with implicants yielding Equation~\eqref{eq:so_implication}
}\label{fig:so_karnaugh}
\end{figure}

\begin{figure}[H]
\centering
\begin{karnaugh-map}[4][2][1][\(x_{i}y_{i}\)][\(c_{i}\)]
\minterms{3,5,6,7}
\autoterms[0]
\implicant{5}{5}
\implicant{3}{7}
\implicant{6}{6}
\end{karnaugh-map}
\caption{
	Karnaugh map for the full-adder function \(c_{o}\), with implicants yielding Equation~\eqref{eq:co_implication}.
	These implicants are purposefully not the largest that could be drawn to make the boolean algebra yielding Equation~\eqref{eq:co_equation} easier.
}\label{fig:co_karnaugh}
\end{figure}

Logic circuits can then be drawn for the resultant Equations~\ref{eq:so_equation}~\&~\ref{eq:co_equation} as has been done in Figures~\ref{fig:so_circuit}~\&~\ref{fig:co_circuit}.
Combining these circuits to reuse the intermediate outputs in an efficient manner yields the circuit for the full-adder, detailed in Figure~\ref{fig:full_adder_circuit}.

\begin{figure}[H]
	\centering
	\begin{circuitikz}[american]
		\draw
		(0,0) node[xor port](xor1) {}
		++(2,0.5) node[xor port](xor2) {}
		(xor1.in 1) ++(-1,0) node[above](xilabel){\(x_{i}\)} node[ocirc](xi) {}
		(xor1.in 2) ++(-1,0) node[above](yilabel){\(y_{i}\)} node[ocirc](yi) {}
		(xor2.in 1) ++(-3,0) node[above](cilabel){\(c_{i}\)} node[ocirc](ci) {}
		(xi) -- (xor1.in 1)
		(yi) -- (xor1.in 2)
		(ci) -- (xor2.in 1)
		(xor1.out) -- (xor2.in 2)
		(xor2.out) ++(1,0) node[above](solabel){\(s_{o}\)} node[ocirc](so) {}
		(xor2.out) -- (so);
	\end{circuitikz}
	\caption{Circuit implementing \(s_{o}\)}\label{fig:so_circuit}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{../img/addergates.png}
	\caption{Circuit implementing \(c_{o}\)}\label{fig:co_circuit}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{../img/Full_adder_ltspice.PNG}
	\caption{
		Full-adder schematic resulting from combination of Figures~\ref{fig:so_circuit}~\&~\ref{fig:co_circuit}
	}\label{fig:full_adder_circuit}
\end{figure}

In the case that multiple bits positions must be added, the ripple-carry adder is the simplest solution.
A ripple-carry adder is constructed out of full-adders with carries placed in sequence, that is the carry-out from the first block is used as the carry-in for the second block, and so on.\autocite[p. 147]{brown_fundamentals_2013}.
This can be scaled for as many bits as needed.
Figure~\ref{fig:ripple_carry_diagram} shows the block-diagram of a four-bit ripple-carry adder.
Figure~\ref{fig:rippleschematic} details how two full-adders would be connected to form a two-bit ripple-carry adder.

\tikzset{
	block/.style={draw, thick, text width=2cm ,minimum height=1.3cm, align=center},
	line/.style={-latex}
}
\begin{figure}[H]
	\begin{tikzpicture}
		%Blocks
		\node[block] (a) {Full Adder A};
		\node[block,left=of a] (b) {Full Adder B};
		\node[block,left=of b] (c) {Full Adder C};
		\node[block,left=of c] (d) {Full Adder D};
		%Bit inputs
		\node[draw, circle] (A1) at ([yshift=2cm, xshift=-0.5cm]$(a)!1.0!(a)$) {A1};
		\node[draw, circle] (A0) at ([yshift=2cm, xshift=0.5cm]$(a)!1.0!(a)$) {A0};
		\node[draw, circle] (B1) at ([yshift=2cm, xshift=-0.5cm]$(b)!1.0!(b)$) {B1};
		\node[draw, circle] (B0) at ([yshift=2cm, xshift=0.5cm]$(b)!1.0!(b)$) {B0};
		\node[draw, circle] (C1) at ([yshift=2cm, xshift=-0.5cm]$(c)!1.0!(c)$) {C1};
		\node[draw, circle] (C0) at ([yshift=2cm, xshift=0.5cm]$(c)!1.0!(c)$) {C0};
		\node[draw, circle] (D1) at ([yshift=2cm, xshift=-0.5cm]$(d)!1.0!(d)$) {D1};
		\node[draw, circle] (D0) at ([yshift=2cm, xshift=0.5cm]$(d)!1.0!(d)$) {D0};
		%Sum and carry outputs
		\node[draw,circle, right=of a] (Ca0) {$C_{0}$};
		\node[draw,circle, left=of d] (C4) {$C_{4}$};
		\node[draw,circle, below=of a] (S0) {S0};
		\node[draw,circle, below=of b] (S1) {S1};
		\node[draw,circle, below=of c] (S2) {S2};
		\node[draw,circle, below=of d] (S3) {S3};
		%Connecting lines
		\draw[line] (a)-- node [midway, above]{$C_{1}$}(b);
		\draw[line] (b)-- node [midway, above]{$C_{2}$}(c);
		\draw[line] (c)-- node [midway, above]{$C_{3}$}(d);
		\draw[line] (A1)--(a);
		\draw[line] (A0)--(a);
		\draw[line] (a)--(S0);
		\draw[line] (B1)--(b);
		\draw[line] (B0)--(b);
		\draw[line] (b)--(S1);
		\draw[line] (C1)--(c);
		\draw[line] (C0)--(c);
		\draw[line] (c)--(S2);
		\draw[line] (D1)--(d);
		\draw[line] (D0)--(d);
		\draw[line] (d)--(S3);
		\draw[line] (Ca0)-- (a);
		\draw[line] (d)--(C4);
	\end{tikzpicture}
	\caption{Ripple Carry Adder Block Diagram}\label{fig:ripple_carry_diagram}
\end{figure}

The ripple-carry adder, while simple to construct, is very slow due to the propagation delay incurred by each adder in sequence.
One remedy to this delay is the carry-look-ahead fast adder.
This circuit uses the resulting value from the lower significant bits to determine if a carry will be required in a higher significant bit.
If a carry is detected, the HIGH value is shorted directly to the higher significant bit.\autocite[p. 146]{brown_fundamentals_2013}
This alternate path allows the final value to be determined without having to wait for the incurring the propagation of the carry bit through all the adders.

Another type of adder is a Binary Coded Decimal (BCD) adder.
This adder is unique because the result from the arithmetic must be adjusted.
Because the BCD value only ranges from 0-9, any result past nine will not be accurate until a value of 6 is added to it.\autocite[p. 290]{kleitz_digital_2012}.
For example, if the binary equivalent of 13 and 6 were added, a normal four-bit adder would produce the following:

\begin{table*}[h!]
	\centering
	\begin{tabular}{c@{\;}c@{\,}c@{\,}c@{\,}c@{\,}}
		  & 1 & 1 & 0 & 1              \\
		+ & 0 & 1 & 1 & 0 \bottomstrut \\
		\hline
		1 & 0 & 0 & 1 & 1 \topstrut    \\
	\end{tabular}
\end{table*}

With a carry of 1.
By adding 6 to this value we obtain:

\begin{table*}[h!]
	\centering
	\begin{tabular}{c@{~}c@{\,}c@{\,}c@{\,}c@{\,}}
		  & 0 & 0 & 1 & 1              \\
		+ & 0 & 1 & 1 & 0 \bottomstrut \\
		\hline
		1 & 1 & 0 & 0 & 1 \topstrut    \\
	\end{tabular}
\end{table*}

Which results in the correct value of 19 in BCD. This could be implemented with a switch that forces a HIGH carry-in for bits 1 and 2, effectively adding 6 to the inputs.


\section*{Methods}

Both the full-adder and ripple-carry adder utilized one of each of the following integrated circuits (ICs):

\begin{itemize}
	\item Texas Instruments SN74AHC86N Quadruple 2-Input Positive XOR Gate
	\item Texas Instruments SN74LS08N Quadruple 2-Input Positive AND Gate
	\item Texas Instruments SNL74LS32N Quadruple 2-Input Positive OR Gate
\end{itemize}

ICs were connected as per Figure~\ref{fig:full_adder_circuit} (See Appendix~C for physical layout schematic) for the full-adder.
ICs were connected as per Figure~\ref{fig:ripple_carry_diagram} for the ripple-carry adder.
Red LEDs were used to indicated a HIGH input \(x_i\), \(y_i\), or \(c_i\).
A green LED was used to indicate \(s_o\), and a white LED was used to indicate \(c_o\) (See Appendix~B for current-limiting resistor calculations and Appendix~C for physical layout schematic).
The inputs \(x_i\) and \(y_i\) were pulsed using Nscope ports P1 and P2, respectively, at 50\% duty cycle.
The carry-in was tied HIGH or LOW according to the stage of the test plan.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{../img/full_adder_breadboard.png}
	\caption{
		Full-adder (Figure~\ref{fig:full_adder_circuit}) as constructed on a breadboard
	}\label{fig:breadboard}
\end{figure}

\subsection*{Full-Adder Test Plan}

\begin{itemize}
	\item Phase 1 was conducted by tying the carry-in HIGH
	\item Phase 2 was conducted by tying the carry in LOW
\end{itemize}

The timing diagram for each phase was compared with the full-adder truth table, Table~\ref{tab:adder_truth}.
An additional verification of the full-adder was completed by testing each possible input individually.

\subsection*{2-Bit Ripple-Carry Adder Test Plan}
A ripple-carry adder was constructed as an extension of the full-adder.
A second full-adder was built using the remaining pins on the ICs.
The new full-adder was tested by comparing each combination of inputs to the truth table as per full-adder test plan.
The carry-out from the first full-adder was then tied into the carry-in for the second.
The gates were wired according to the schematic in Figure~\ref{fig:rippleschematic}.
The green LED indicates the sum of the inputs A0 and A1, the white LED indicates the sum of the inputs B0 and B1, while the red LED indicates a final carry out.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{../img/ripple_adder_ltspice.PNG}
	\caption{Ripple-carry adder constructed from the sequencing of two full-adders.}\label{fig:rippleschematic}
\end{figure}

The timing diagram was studied with different inputs and compared to expected values.
In the snapshot in Figure~\ref{fig:rippletiming},

\begin{figure}[H]
	\includegraphics[width=\textwidth]{../img/ripple_timing.PNG}
	\caption{Timing diagram displaying the inputs and outputs of the two-bit ripple-carry adder as per Figure~\ref{fig:breadboard}}\label{fig:rippletiming}
\end{figure}

Additionally a selection inputs were tested individually for clarity.
See Appendix A for photos of various states on the breadboard.

\section*{Results \& Discussion}

The full-adder successfully counted to one in binary.
The attached Video A shows the full-adder in operation with the carry-in forced LOW.
The truth table matched the timing diagram for all possible inputs.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{../img/truthand_tables.png}
	\caption{Full-Adder Timing Diagram}\label{fig:fulltiming}
\end{figure}

The ripple-carry successfully added to a maximum value of seven.
The attached Video B shows the adder in operation with the initial carry-in forced LOW.
The inputs for the second bit are shared with the values for the first bit.

The full-adder circuit could be modified to subtract.
In order to subtract a binary number, first the two's complement is applied.
A signal that indicates a negative number is required initiates an inverting circuit that takes the complement of the inputs using NOT-gates.
The trigger also sends a HIGH to the carry-in of the first full-adder, effectively adding a 1.\autocite[p. 288]{kleitz_digital_2012}
This causes the value to transform into the two's complement equivalent.
When the negative value is added, the final result is equivalent to subtracting.

\section*{Conclusion}
By using discrete gates, one can build an adder for as many bits as desired.
Both a full-adder and two-bit ripple-carry adder were constructed and characterized, with each confirmed to operate according to the discussed theory.
Modifications to these circuits were discussed to produce other relevant logic in the domain of binary arithmetic.
Shortcomings specifically with the ripple-carry adder were discussed, and a remedy given by the carry-look-ahead fast adder mentioned.

Complex modern systems are built from several constituent small modules including full adders and the relevance of theory and characterization is ever-increasing in a more digital world.

\printbibliography
\includepdf[pages=-]{appendix_a.pdf}
\includepdf[pages=-]{appendix_b.pdf}
\includepdf[pages=-]{appendix_c.pdf}
\end{document}
